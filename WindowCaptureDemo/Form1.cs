﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowCaptureDemo
{
    public partial class Form1 : Form
    {
        WinAPI _api;
        GDIIMAGE _gdi;
        FloatWindow _floatForm;
        int cutwidth;
        int cutheight;
        Graphics g;
        public Form1()
        {
            InitializeComponent();

            _floatForm = new FloatWindow();
            WindowContainer container = new WindowContainer(_floatForm.ProgramHost, string.Empty);
            string programsPath = Application.StartupPath + "\\GetVideo.exe";
            container.Start(programsPath);
            _floatForm.Show();
            //_api = new WinAPI();
            _gdi = new GDIIMAGE();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           

            //var  img = _gdi.CaptureWindow(_floatForm.ProgramHost.Handle);

            var img = GDIIMAGE_PLUS.GetWindow(_floatForm.ProgramHost.Handle);
            Bitmap destMap = new Bitmap(img.Width,img.Height - 48);
            Rectangle destRect = new Rectangle(0,0, img.Width, img.Height - 48);
            Rectangle srcRect = new Rectangle(0,24,img.Width,img.Height - 48);
            Graphics g = Graphics.FromImage(destMap);
            g.DrawImage(img,destRect,srcRect,GraphicsUnit.Pixel);
            destMap.Save("777.jpg");
        }
    }
}
